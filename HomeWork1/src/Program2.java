/**
 * 05.10.2020
 * HomeWork1
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Program2 {
    public static int fact(int n) {
        System.out.println("->> fact with " + n);
        if (n == 0) {
            return 1;
        }
        return fact(n - 1) * n;
    }
    public static void main(String[] args) {
        int number = fact(5);
        System.out.println(number);
    }
}
